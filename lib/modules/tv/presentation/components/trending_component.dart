import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/presentation/components/tv_slide_view.dart';
import '../../data/data_source/tv_data_source.dart';
import '../../data/models/tv_model.dart';


class TrendingComponent extends StatelessWidget {
  const TrendingComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SizedBox(
      width: double.infinity,
      height: size.width*0.5,
      child: FutureBuilder<List<TvModel>>(
          future: TvDataSource.getTrending(),
          builder: (context,snapShot) {
            if(snapShot.hasData){
              return CarouselSlider.builder(
                options: CarouselOptions(
                  initialPage: snapShot.data!.length~/2,
                  autoPlay: true,
                ),
                itemCount: snapShot.data!.length,
                itemBuilder: (BuildContext context, int index, int realIndex)
                    => TvSlideView(snapShot.data![index]),
              );
            }
            if(snapShot.hasError){
              return Center(
                  child: Text(snapShot.error.toString())
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }
      ),
    );
  }
}
