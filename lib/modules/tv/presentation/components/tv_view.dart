import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/data/models/tv_model.dart';
import 'package:my_tv/modules/tv/presentation/screens/tv_screen.dart';

import '../../../../core/const/api_const.dart';

class TvView extends StatelessWidget {
  final TvModel model;
  const TvView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var shadowColor =
    (theme.brightness == Brightness.dark)?
        Colors.white:
        Colors.black;
    return GestureDetector(
      onTap: (){
        Navigator.push(context,
          MaterialPageRoute(
              builder: (context) =>
                  TvScreen(model),
          )
        );
      },
      child: Container(
        margin: EdgeInsets.all(size.width*0.025),
        decoration: BoxDecoration(
          color: theme.cardColor,
          borderRadius: BorderRadius.circular(size.width*0.025),
          image: DecorationImage(
              image: NetworkImage(
                ApiConst.image(model.posterPath),
              )
          ),
          boxShadow: [
            BoxShadow(
              color: shadowColor.withOpacity(0.5),
              offset: Offset(1.5,1.5),
              spreadRadius: 2,
              blurRadius: 2
            ),
            BoxShadow(
              color: shadowColor.withOpacity(0.5),
              offset: Offset(-1.5,-1.5),
              spreadRadius: 2,
              blurRadius: 2
            ),
          ]
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                margin: EdgeInsets.all(size.width*0.025),
                padding: EdgeInsets.all(size.width*0.025),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(size.width*0.01),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Icons.star,color: Colors.amber,),
                    SizedBox(
                      width: size.width*0.025,
                    ),
                    Text(
                        '${model.voteAverage}/10',
                          style: const TextStyle(
                            color: Colors.black
                          ),
                    ),
                  ],
                )
            ),
            Container(
                height: size.width*0.2,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    colors: [
                      Colors.black87,
                      Colors.transparent
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter
                  ),
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(size.width*0.025)
                  )
                ),
                padding: EdgeInsets.all(size.width*0.025).copyWith(
                  top: size.width*0.05
                ),
                child: Text(
                    model.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),
                ),
            ),
          ],
        ),
      ),
    );
  }
}
