import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/data/models/tv_model.dart';
import 'package:my_tv/modules/tv/presentation/screens/tv_screen.dart';
import '../../../../core/const/api_const.dart';

class TvSearchView extends StatelessWidget {
  final TvModel model;
  const TvSearchView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(model.name),
        subtitle: Text(model.firstAirDate),
        leading: CircleAvatar(
          foregroundImage: NetworkImage(
            ApiConst.image(model.posterPath),
          ),
        ),
        trailing: const Icon(Icons.info_outline),
        onTap: (){
          Navigator.push(context,
              MaterialPageRoute(
                builder: (context) =>
                    TvScreen(model),
              )
          );
        },
      ),
    );
  }
}
