import 'package:flutter/material.dart';
import 'package:my_tv/core/const/api_const.dart';
import 'package:my_tv/modules/tv/presentation/screens/tv_screen.dart';

import '../../data/models/tv_model.dart';

class TvSlideView extends StatelessWidget {
  final TvModel model;
  const TvSlideView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
          TvScreen(model),
        )
      ),
      child: Container(
        width: double.infinity,
        height: double.infinity,
        margin: EdgeInsets.all(0.025 * size.width),
        decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(ApiConst.image(model.posterPath)),
              fit: BoxFit.cover
            )
        ),
        child: Column(
          children: [
            const Spacer(),
            Expanded(
                child: Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: double.infinity,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            Colors.black87,
                            Colors.transparent
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter
                      )
                  ),
                  child: Text(
                      model.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                      ),
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }
}
