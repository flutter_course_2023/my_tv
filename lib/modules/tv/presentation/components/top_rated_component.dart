import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/presentation/components/tv_view.dart';

import '../../data/data_source/tv_data_source.dart';
import '../../data/models/tv_model.dart';

class TopRatedComponent extends StatelessWidget {
  const TopRatedComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<TvModel>>(
        future: TvDataSource.getTopRated(),
        builder: (context,snapShot) {
          if(snapShot.hasData){
            return SliverGrid(
                delegate: SliverChildBuilderDelegate(
                        (context,i) {
                           print(i);
                           return TvView(snapShot.data![i]);
                        },
                  childCount: snapShot.data!.length,
                ),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: .7
            ),
            );

            // return GridView.builder(
            //   // shrinkWrap: true,
            //   // physics: const PageScrollPhysics(),
            //   itemCount: snapShot.data!.length,
            //   itemBuilder: (context,i) {
            //     print(i);
            //     return TvView(snapShot.data![i]);
            //   },
            //   gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            //       crossAxisCount: 2,
            //       // crossAxisSpacing: 20,
            //       // mainAxisSpacing: 20,
            //       childAspectRatio: .7
            //   ),
            // );
          }
          if(snapShot.hasError){
            return SliverToBoxAdapter(
                child: Center(
                    child: Text(snapShot.error.toString())
                )
            );
          }
          return const SliverToBoxAdapter(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
    );
  }
}
