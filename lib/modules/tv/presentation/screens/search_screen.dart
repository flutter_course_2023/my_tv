import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/data/data_source/tv_data_source.dart';
import 'package:my_tv/modules/tv/data/models/tv_model.dart';
import 'package:my_tv/modules/tv/presentation/components/tv_search_view.dart';

class TvSearchDelegate extends SearchDelegate{


  @override
  Widget buildSuggestions(BuildContext context) {
    return const SizedBox();
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
        IconButton(
           onPressed: (){
             query = '';
           },
           icon: const Icon(Icons.delete)
       )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () => Navigator.pop(context),
        icon: const Icon(Icons.close)
    );
  }

  @override
  Widget buildResults(BuildContext context) {
        return FutureBuilder<List<TvModel>>(
            future: TvDataSource.search(query),
            builder: (context,snapShot){
              if(snapShot.hasData){
                return ListView.builder(
                    itemCount: snapShot.data!.length,
                    itemBuilder: (context,i) =>
                        TvSearchView(snapShot.data![i]),
                );
              }
              if(snapShot.hasError){
                return Center(
                  child: Text(snapShot.error.toString()),
                );
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
        );
  }


}