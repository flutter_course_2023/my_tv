import 'package:flutter/material.dart';
import '../../../../core/const/api_const.dart';
import '../../data/models/tv_model.dart';

class TvScreen extends StatelessWidget {
  final TvModel model;
  const TvScreen(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: size.width*0.65,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(model.name),
              //centerTitle: false,
              background: SizedBox(
                width: double.infinity,
                height: size.width*0.65,
                child: Stack(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      height: size.width*0.72,
                      child: Image.network(
                        ApiConst.image(model.backdropPath),
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: size.width*0.72,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Colors.black54,
                              Colors.transparent
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter
                        )
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate(
                  [
                    _buildKey('Original Name', size),
                    _buildValue(model.originalName, size),
                    _buildKey('Origin Country', size),
                    _buildValue(model.originCountry, size),
                    _buildKey('Original Language', size),
                    _buildValue(model.originalLanguage, size),
                    _buildKey('First Show', size),
                    _buildValue(model.firstAirDate, size),
                    _buildKey('Vote Average', size),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal:size.width*0.025),
                      child: Row(
                        children: [
                          Text('${model.voteAverage}/10',
                            style: const TextStyle(
                                fontSize: 14
                            ),
                          ),
                          SizedBox(
                            width: size.width*0.025,
                          ),
                          Icon(Icons.star,color: Colors.amber,),
                        ],
                      ),
                    ),
                    _buildKey('Vote Count', size),
                    _buildValue('${model.voteCount} people voted on this', size),
                    _buildKey('Overview', size),
                    _buildValue(model.overview, size),
                    SizedBox(
                      height: size.width*0.075,
                    ),
                  ]
              )
          )
        ],
      ),
    );
  }
  
  Widget _buildKey(String key,Size size) =>
      Padding(
        padding: EdgeInsets.all(size.width*0.025),
        child: Text(key,
          style: TextStyle(
            color: Colors.blue[800],
            fontWeight: FontWeight.bold,
            fontSize: 16
          ),
        ),
      );

  Widget _buildValue(String value,Size size) =>
      Padding(
        padding: EdgeInsets.symmetric(horizontal:size.width*0.025),
        child: Text(value,
          style: const TextStyle(
              fontSize: 14
          ),
        ),
      );
      
}


