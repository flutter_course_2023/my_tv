import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/presentation/components/tv_view.dart';

import '../../data/data_source/tv_data_source.dart';
import '../../data/models/tv_model.dart';
import '../components/top_rated_component.dart';
import '../components/trending_component.dart';
import 'search_screen.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  // List<TvModel> data = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: [
          SliverAppBar(
            title: const Text('Home'),
            pinned: true,
            actions: [
              IconButton(
                  onPressed: ()=>
                    showSearch(
                        context: context,
                        delegate: TvSearchDelegate()
                    ),
                  icon: const Icon(Icons.search)
              )
            ],
          ),
          const SliverToBoxAdapter(
            child: TrendingComponent(),
          ),
          TopRatedComponent(),
        ],
      ),
    );
  }
}
