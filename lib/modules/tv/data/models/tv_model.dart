
class TvModel {

  final String backdropPath;
  final String posterPath;
  final String firstAirDate;
  final String name;
  final String originCountry;
  final String originalLanguage;
  final String originalName;
  final String overview;
  final num voteAverage;
  final int voteCount;

  TvModel({
      required this.backdropPath,
      required this.posterPath,
      required this.firstAirDate,
      required this.name,
      required this.originCountry,
      required this.originalLanguage,
      required this.originalName,
      required this.overview,
      required this.voteAverage,
      required this.voteCount
  });

  factory TvModel.fromJson(Map<String,dynamic> json) =>
      TvModel(
          backdropPath: json['backdrop_path'],
          posterPath: json['poster_path'],
          firstAirDate: json['first_air_date'],
          name: json['name'] ?? '',
          originCountry: ((json['origin_country'] ?? []) as List).join(" , "),
          originalLanguage: json['original_language'] ?? 'En',
          originalName: json['original_name'],
          overview: json['overview'],
          voteAverage: json['vote_average'],
          voteCount: json['vote_count']
      );
}