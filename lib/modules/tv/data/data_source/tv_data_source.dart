import 'dart:async';
import 'package:my_tv/core/const/api_const.dart';
import 'package:my_tv/core/network/network_helper.dart';
import 'package:my_tv/modules/tv/data/models/tv_model.dart';


class TvDataSource {

  static Future<List<TvModel>> getTopRated()async{
    var response = await NetworkHelper().get(ApiConst.topTvs);
    Map<String,dynamic> json = response.body;
    List result = json['result'];
    return result.map(
            (e) =>
                TvModel.fromJson(e)
    ).toList();
  }


  static Future<List<TvModel>> getTrending()async{
    var response = await NetworkHelper().get(ApiConst.trendingTVs);
    Map<String,dynamic> json = response.body;
    List result = json['result'];
    return result.map(
            (e) =>
            TvModel.fromJson(e)
    ).toList();
  }


  static Future<List<TvModel>> search(String query)async{
    var response = await NetworkHelper().get(
        ApiConst.search(query),
        headers: {
          'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmMDhjY2QxM2JkNWVhODRmNzYxYTMxZjYzNjBlYTQ5YyIsInN1YiI6IjYzY2JiZDZhY2VlNDgxMDBhNDQ3OWE1YiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.krMQ3Qhi11Nf2nOUrhKHXm_P9SOfnd6UE4Z0srkAHm4'
        }
    );
    Map<String,dynamic> json = response.body;
    List result = json['results'];
    return result.map(
            (e) =>
            TvModel.fromJson(e)
    ).toList();
  }

}