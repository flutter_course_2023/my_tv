import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:my_tv/core/const/assets_const.dart';
import 'package:sizer/sizer.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen> {



  void _splashToHome(){
    Future.delayed(const Duration(seconds: 3)).then(
            (_) =>
                Navigator.pushNamedAndRemoveUntil(context, '/home',(_) => false)
    );
  }

  @override
  void initState() {
    super.initState();
    _splashToHome();
  }

  @override
  Widget build(BuildContext context) {
    //MediaQuery.of(context);

    return Scaffold(
      body: Center(
        child: CircleAvatar(
            radius: 25.w,
            backgroundColor: Colors.white,
            child: Lottie.asset(AnimationConst.tv),
        ),
      ),
    );
  }

}
