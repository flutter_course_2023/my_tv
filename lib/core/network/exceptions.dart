
class NetworkException implements Exception {
  final String message;
  NetworkException([this.message = 'fail']);

  @override
  String toString() {
    return message;
  }
}