import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:my_tv/core/network/exceptions.dart';

class NetworkHelper {

  static const _timeLimit = Duration(
    seconds: 25
  );

  NetworkHelper._();
  static NetworkHelper? _instance;
  factory NetworkHelper() => _instance ??= NetworkHelper._();

  Future<AppResponse> get(
      String url,
      {
        Duration? timeOut,
        Map<String,String>? headers
      }) async{
    try{
      var response = await http.get(
          Uri.parse(url),
          headers: {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
            if(headers != null)
              ...headers,
          }
      ).timeout(timeOut ?? _timeLimit);

      if(response.statusCode >= 300 || response.statusCode < 200) {
        throw NetworkException('fail with code : ${response.statusCode}');
      }

      return AppResponse.fromHttp(response);

    }on NetworkException{
      rethrow;
    }
    on TimeoutException{
      throw NetworkException('fail , time out');
    }
    catch(e){
      print(e);
      throw NetworkException('fail , something went wrong!');
    }
  }

  Future<AppResponse> post(
      String url,
      {
        Duration? timeOut,
        Map<String,String>? headers,
        Map<String,dynamic>? body,
      }) async{
    try{
      var response = await http.post(
          Uri.parse(url),
          headers: {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
            if(headers != null)
              ...headers,
          },
          body: jsonEncode(body ?? {}),
      ).timeout(timeOut ?? _timeLimit);

      if(response.statusCode >= 300 || response.statusCode < 200) {
        throw NetworkException('fail with code : ${response.statusCode}');
      }

      return AppResponse.fromHttp(response);

    }on NetworkException{
      rethrow;
    }
    on TimeoutException{
      throw NetworkException('fail , time out');
    }
    catch(e){
      throw NetworkException('fail , something went wrong!');
    }
  }


  Future<AppResponse> put(
      String url,
      {
        Duration? timeOut,
        Map<String,String>? headers,
        Map<String,dynamic>? body,
      }) async{
    try{
      var response = await http.put(
        Uri.parse(url),
        headers: {
          'Content-Type' : 'application/json',
          'Accept' : 'application/json',
          if(headers != null)
            ...headers,
        },
        body: jsonEncode(body ?? {}),
      ).timeout(timeOut ?? _timeLimit);

      if(response.statusCode >= 300 || response.statusCode < 200) {
        throw NetworkException('fail with code : ${response.statusCode}');
      }

      return AppResponse.fromHttp(response);

    }on NetworkException{
      rethrow;
    }
    on TimeoutException{
      throw NetworkException('fail , time out');
    }
    catch(e){

      throw NetworkException('fail , something went wrong!');
    }
  }

  Future<AppResponse> delete(
      String url,
      {
        Duration? timeOut,
        Map<String,String>? headers,
        Map<String,dynamic>? body,
      }) async{
    try{
      var response = await http.delete(
        Uri.parse(url),
        headers: {
          'Content-Type' : 'application/json',
          'Accept' : 'application/json',
          if(headers != null)
            ...headers,
        },
        body: jsonEncode(body ?? {}),
      ).timeout(timeOut ?? _timeLimit);

      if(response.statusCode >= 300 || response.statusCode < 200) {
        throw NetworkException('fail with code : ${response.statusCode}');
      }

      return AppResponse.fromHttp(response);

    }on NetworkException{
      rethrow;
    }
    on TimeoutException{
      throw NetworkException('fail , time out');
    }
    catch(e){
      throw NetworkException('fail , something went wrong!');
    }
  }

}


class AppResponse {

  final Map<String,String> headers;
  final dynamic body;
  final int statusCode;

  AppResponse({
    required this.headers,
    required this.body,
    required this.statusCode
  });

  factory AppResponse.fromHttp(http.Response response) =>
    AppResponse(
        headers: response.headers,
        body: jsonDecode(response.body),
        statusCode: response.statusCode
    );
}