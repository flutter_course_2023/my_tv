
class ApiConst {

  static const _baseUrl = 'https://7ddc6e7e-d9ff-4c52-8756-0335f6b50ae5.mock.pstmn.io';

  static const topTvs = '$_baseUrl/tv/top_rated';

  static const trendingTVs = '$_baseUrl/tv/trending';

  static String image(String path) => 'https://image.tmdb.org/t/p/original$path';

  static String search(String query) =>
      'https://api.themoviedb.org/3/search/tv?query=$query';


}