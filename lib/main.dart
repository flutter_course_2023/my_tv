import 'package:flutter/material.dart';
import 'package:my_tv/modules/tv/presentation/screens/home_screen.dart';
import 'package:my_tv/modules/tv/presentation/screens/tv_screen.dart';
import 'package:sizer/sizer.dart';

import 'modules/splash/presentation/screens/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (_,__,___) =>
          MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              appBarTheme: AppBarTheme(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.red.withOpacity(0.8),
              ),
              //scaffoldBackgroundColor: Colors.grey
              primaryColor: Colors.red,
              colorScheme: ColorScheme.fromSwatch(
                primarySwatch: Colors.red,
                brightness: Brightness.light,
              ),
              cardColor: Colors.white,
            ),
            darkTheme: ThemeData(
              appBarTheme: const AppBarTheme(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.black54,
              ),
              //scaffoldBackgroundColor: Colors.grey
              primaryColor: Colors.blueGrey,
              colorScheme: ColorScheme.fromSwatch(
                primarySwatch: Colors.blueGrey,
                brightness: Brightness.dark,
              ),
              cardColor: Colors.grey,
            ),
            themeMode: ThemeMode.system,
            routes: {
              '/home': (c) => const HomeScreen(),
              '/': (c) => SplashScreen()
            },
          ),
    );
  }
}
